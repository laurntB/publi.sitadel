---
title: ""
lang: fr
format: 
  html:
    css: "./css/styles.css"
    df-print: kable
    fig-width: 8
    fig-height: 4.5
    toc: true
    toc-location: right
execute: 
  echo: false
  warning : false
---



```{r}
#| label: chargement des données et des libraries
## source('./configuration_locale_offline.R')
source('./configuration_locale.R')
source('./prjlib_connexes.R')

## chargement des données
### Données sitadel sélectionnées : préparation indicateurs
load(file.path(DATASTORE, SRCDIR_PUBLI, fn_donnees_preparees))

source("./indicateurs.R")
```

`r datasetinfo$territoire`



# Le nombre de logements autorisés en nette progression en [`r annee_etu`]{.publi-date}

En [`r annee_etu`]{.publi-date}, 
[`r fmt(s.aut.nb(annee_etu))`]{.publi-data} logements ont été autorisés à la 
construction en [`r libreg`]{.publi-location}, 
en progression de [`r s.aut.evo.prct()`]{.publi-data}% par rapport 
à [`r annee_nm1`]{.publi-date}, après une augmentation de 
[`r s.aut.evo_nm1.prct()`]{.publi-data}% entre 
[`r annee_nm1-1`]{.publi-date} et [`r annee_nm1`]{.publi-date}. 
Dans le même temps, le nombre de logements autorisés 
en France (hors Mayotte) est en repli de
[`r s.hx.aut.evo.prct(annee_etu)`]{.publi-data}%.

De [`r annee_debpttprd`]{.publi-date} à [`r annee_etu`]{.publi-date},
le nombre de logements autorisés 
a été de [`r fmt(s.aut.nb.a01(annee_etu,annee_debpttprd))`]{.publi-data} unités, soit 
[`r fmt(s.aut.nb.a02(annee_etu,annee_debpttprd))`]{.publi-data} logements en 
moyenne par an. Les résultats de l'année [`r annee_etu`]{.publi-date}
se situent parmi les plus élevés de la dernière décennie,
après ceux de [2010]{.question} et [2015]{.question}.

Parmi les [`r fmt(s.aut.nb(annee_etu))`]{.publi-data} logements autorisés
en [`r annee_etu`]{.publi-date}, 
[`r s.aut.prop.ind(annee_etu)`]{.publi-data}% sont des logements individuels et
[`r s.aut.prop.colres(annee_etu)`]{.publi-data}% 
des logements collectifs. Ces deux derniers chiffres diffèrent de la moyenne 
sur les cinq dernières années puisque
les logements individuels ont représenté [`r s.aut.propmoy5a.ind(annee_etu)`]{.publi-data}%
des autorisations délivrées et
les logements collectifs [`r s.aut.propmoy5a.colres(annee_etu)`]{.publi-data}%.

Parmi les principaux résultats de cette publication sur la construction 
en [`r libreg`]{.publi-location} en [`r annee_etu`]{.publi-date}, 
l'évolution des chiffres de la construction est analysée 
pour les communes du Lamentin et de Fort-de-France, 
ainsi que l'évolution de la taille des logements autorisés ces dernières années, 
en baisse continuelle. Concernant le logement locatif social, 
le rythme de construction a ralenti en [`r annee_etu`]{.publi-date}, 
mais reste élevé par rapport au niveau national.


## Principaux résultats sur les logements autorisés

Voici tout d'abord les résultats entre [`r annee_nm1`]{.publi-date} et 
[`r annee_etu`]{.publi-date} :


```{r}
#| label: tbl-1
#| tbl-cap: !expr 'paste("Chiffres clés du logement en",annee_etu)'

data4kable <- dp_tb_aut(indicateurs_annuels_dom_aut,annee_nm1,annee_etu)


data4kable %>%     
  knitr::kable(
    col.names = append(c(""), colnames(data4kable[2:3]))
  )
```



Les chiffres obtenus ci-dessus sont issus du Service de la Donnée et 
des Études Statistiques (SDES), en suivant la méthodologie 
des «Estimations en date réelle des logements autorisés et commencés» 
(voir dernière page de la publication) de [mars 2016].

Les tableaux et les graphiques ci-dessous illustrent les évolutions de la 
construction entre [`r annee_debgrdprd`]{.publi-date} et [`r annee_etu`]{.publi-date}, 
pour les logements autorisés d'abord, pour les logements commencés ensuite :


```{r}
#| label: tbl-2
#| tbl-cap: !expr 'sprintf("Nombre de logements autorisés entre %d et %d", annee_debgrdprd, annee_etu)'

data4kable <- dp_tb_aut_compare_hexagone(
  indicateurs_annuels_dom_aut, 
  indicateurs_annuels_territoire_national_horsMayotte_aut,
  annee_debgrdprd,annee_etu,
  datasetinfo
)

data4kable %>% 
  knitr::kable(
    col.names = append(c(""), colnames(data4kable)[2:4]),
    format.args = list(
      big.mark = " ",
      digits = 2
    )
  )
```




```{r}
#| label: fig-1
#| fig-cap: !expr 'sprintf("Evolution du nombre de logements autorisés entre %d et %d", annee_debgrdprd, annee_etu)'
chrb100 <- dp_gl_aut_compare_hexagone(
  indicateurs_annuels_dom_aut,
  indicateurs_annuels_territoire_national_horsMayotte_aut,
  annee_debgrdprd,
  annee_etu,
  datasetinfo
)

ggplot() +
  geom_line(data = chrb100$dom, aes(x=annee, y=b100), color = "red") +
  geom_line(data = chrb100$hx, aes(x=annee, y=b100), color = "blue") +
  scale_x_continuous(breaks = seq(annee_debgrdprd,2022), minor_breaks = FALSE)


```



Concernant le type de logements, la carte ci-dessous permet de donner 
la répartition par commune.



```{r}
#| label: carte
#| fig-cap: !expr "sprintf('Logements autorisés par commune en %s', annee_etu)"
#| message: false
#| warning: false
#| fig-width: 15
#| fig-height: 25


ftrsfrm <- function(x) x

##  adapté martinique >>>
ftrsfrm <- function(x) log(x*x+50)*200
flabeller <- function(x) round(  sqrt( exp((x/200)) - 50 ) , digits = 0) # flabeller(radius)
steps <- c(10,125,1000)
radius <- ftrsfrm(steps)
##  <<< adapté martinique 



indicateurs_xy <- dp_mp_aut_indics_tpl(rlgts_dom, communes_geo, annee_etu)

ggplot() + 
  geom_sf(data=communes_geo) +
  geom_scatterpie(
    ## aes(x=x, y=y),  
    ##  adapté martinique >>> 
    aes(x=x, y=y, r=logtot),
    ##  <<< adapté martinique 
    data=indicateurs_xy, 
    cols=c('ind','colres')
  )+
  geom_text(data= indicateurs_xy,aes(label=NOM, x=x, y=y), size=2)+ 
  ##  adapté martinique >>>
  geom_scatterpie_legend(
    x = 700000,      # 700000
    y = 1600000,     # 1590000
    breaks = radius,
    labeller = flabeller,
    label_position = "left"
  ) +
  ##  <<< adapté martinique
  theme_void() +
  theme(
    legend.position = c(0.1,0.5)
  ) + 
  scale_fill_manual(
    values=c("#E69F00", "#56B4E9"), 
    name="Par communes, \n proportion de logements",
    breaks=c("ind", "colres"),
    labels=c("individuels", "collectifs et en résidences")
  )

```



[Fort-de-France a été la commune ayant concentré le plus de logements autorisés 
en 2018 en Martinique avec 32% du nombre total, loin devant Ducos (10%) et le Lamentin (7%). Ces trois communes ont regroupé 49% des logements autorisés pour 138500 habitants, soit 37% de la population martiniquaise (source: Recensement de la Population 2016, INSEE). Le rythme de construction sur ce territoire est donc au-dessus la moyenne martiniquaise.]{.publi-hightlight}

[Voyons à présent le détail des deux villes les plus peuplées de Martinique, à savoir le Lamentin et Fort-de-France:]{.publi-hightlight}



```{r}
#| label: fig-2
#| fig-cap: !expr "sprintf('Evolution du nombre de logements autorisés de %s à %s', annee_debpttprd, annee_etu)"

dp_gc_aut_inter2com( rlgts_dom,
            communes_geo,
            com1=communes[[1]], com2=communes[[2]],
	          deb=annee_debpttprd,
            fin=annee_etu
)%>% 
  ggplot() +
  geom_col(
    aes(x=anneechr, y=nb.aut, group=NOM, fill=NOM),
    position = "dodge"
  )  +
  ##  adapté martinique >>>
  scale_fill_manual(
    values = c(
      "Fort-de-France"="orange",
      "Le Lamentin"="darkgreen"
    )
  )
  ##  <<< adapté martinique
```



[Les dynamiques d'urbanisme différent entre ces deux communes sur les cinq 
dernières années. Un ralentissement était observé sur Fort-de-France 
jusqu'en  [`r annee_nm1`]{.publi-date}, avant un fort rebond en 
[`r annee_etu`]{.publi-date}. 
En effet, plus de [1100]{.publi-data}logements ont été autorisés, 
dont [95]{.publi-data}% sont des logements collectifs. 
Il y a ainsi eu [10]{.publi-data} projets de [50]{.publi-data} logements et plus
autorisés en [`r annee_etu`]{.publi-date}. Ce résultat intéressant est à mettre
en parallèle de la déprise démographique élevée sur Fort-De-France:
moins[9]{.publi-data}% sur les cinq dernières années. Cette nouvelle offre de 
logements est à suivre avec attention car la part des logements vacants a 
explosé ces dernières années, progressant de [13,6]{.publi-data}% 
à [17,5]{.publi-data}% de [2011]{.publi-data} à [2016]{.publi-data}.]{.publi-hightlight}

[D'autre part, en ramenant les résultats à la population ([81000]{.publi-data} 
à Fort-de-France et [40000]{.publi-data} au Lamentin (source INSEE 2016), 
on obtient un nombre de logements autorisés à Fort-de-France 
de [3,9]{.publi-data} pour 100 habitants sur 5 ans 
(dans la moyenne martiniquaise), contre [5,2]{.publi-data} au Lamentin, signe 
d'une activité intense du secteur de la construction sur ce territoire.]{.publi-hightlight}





## **Évolution du nombre de logements autorisés selon les EPCI**

Entre  [`r annee_nm1`]{.publi-date} et [`r annee_etu`]{.publi-date}, les trois 
communautés d'agglomération du territoire ont vu leurs répartitions 
d'autorisations de construire rester sensiblement les mêmes :

```{r}
#| label: commentaires automatisés
#| results: asis
generateur_liste_commentaires_epci(annee_etu) %>% cat()
```



```{r}
#| label: fig-3
#| fig-cap: !expr 'sprintf("Logements autorisés par EPCI de %d à %d", annee_debgrdprd, annee_etu)'


dp_gl_aut_interepci(
    rlgts_dom,
    communes_geo,
    annee_debgrdprd,
    annee.etu,
    annee.ref = 2013
)  %>% 
  inner_join(
    correspondance_sirenepci_libelle,
    by=c("SIREN_EPCI"="code")
  ) %>%
  ggplot() +
  geom_line(
    aes(x=annee.aut, y=b100, group=nom, color=nom)
  ) 
# +
#   scale_color_manual(
#     values = c(
#       "200041788"="red",
#       "249720053"="orange",
#       "249720061"="darkgreen"
#     ),
#     labels = c(
#       "200041788"="red",
#       "249720053"="orange",
#       "249720061"="darkgreen"
#     ),
#   )
```


[Depuis [2009]{.publi-data}, la répartition du nombre de logements autorisés 
entre les 
communautés d'agglomérations a fluctué. La CACEM a connu un rythme plus soutenu 
que les autres EPCI, porté par la dynamique de Fort-de-France en 
[`r annee_etu`]{.publi-date}.]{.publi-hightlight}



## Des logements autorisés plus nombreux mais plus petits en superficie

En [`r annee_etu`]{.publi-date}, [`r s.aut.surf.tot()`]{.publi-data}m² de surfaces de 
plancher de logements sont enregistrées dans 
le département. Comme le nombre de logements autorisés a progressé de 
[`r s.aut.evo.prct()`]{.publi-data}%
depuis un an, cette superficie connaît une hausse similaire de 
[`r s.aut.surf.evo()`]{.publi-data}% par rapport à  [`r annee_nm1`]{.publi-date}. 
En revanche, la surface moyenne de plancher des logements autorisés continue de 
baisser : elles est ainsi passée 
de         [`r s.aut.surf.moy(2016)`]{.publi-data}m²([2016]{.publi-date}) 
à          [`r s.aut.surf.moy(annee_nm1)`]{.publi-data}m²( [`r annee_nm1`]{.publi-date}) 
et atteint [`r s.aut.surf.moy(annee_etu)`]{.publi-data}m² en [`r annee_etu`]{.publi-date} 


## **Répartition des logements selon le nombre de pièces**

L'évolution des logements autorisés selon leur nombre de pièces est donnée via 
l'histogramme empilé ci-dessous :



```{r}
#| label: fig-4
#| fig-cap: !expr "sprintf('Evolution de la répartition selon le nombre de pièce des projets de construction entre %s et %s', annee_debpttprd, annee_etu)"
dp_gc_aut_nbpieceslogement(
  rlgts_dom,
  communes_geo,
  annee_debpttprd,
  annee_etu
) %>% 
  ggplot(aes(x=annee.aut, y = prcts, group = type)) + 
  geom_col(
    aes(fill=type)
  )+ 
  geom_text(
    aes(label=glue::glue("{val}%",val=round(prcts,1))),
    position = position_stack(vjust = 0.5)
  )


```






La structure selon la taille des logements autorisés met en évidence la 
prédominance des logements de 3 et 4 pièces. 
En [`r annee_nm1`]{.publi-date}  , 
les T3 et T4 représentent [`r s.aut.part.typ3et4()`]{.publi-data} % des logements autorisés.

[La part élevée des T1 et T2 pour les autorisations en [`r annee_etu`]{.publi-date}   
s'explique directement par la proportion élevée des logements collectifs cette 
même année.]{.publi-hightlight} 

Enfin, la part des plus grands 
([`r s.aut.part.typ5etplus()`]{.publi-data} % pour les T5 ou plus) 
reste sensiblement la même par rapport à l'année précédente.





## **Quelques éléments identifiant les logements**

[Comme en [`r annee_nm1`]{.publi-date}  , 
[`r s.aut.prop.nvllecstr()`]{.publi-data} % des logements 
autorisés sont de nouvelles constructions en [`r annee_etu`]{.publi-date}. 
Ces chiffres sont stables sur les cinq dernières années.]{.publi-hightlight} 




```{r}
#| label: fig-5
#| fig-cap: !expr "sprintf('Evolution de la de la nature des projets liés aux logements autorisés entre %s et %s', annee_debpttprd, annee_etu)"
dp_gc_aut_natureprojetdeclare(rlgts_dom, deb=annee_debpttprd, fin=annee_etu) %>% 
  ggplot(aes(x=annee.aut, y = pourcentage, group = type)) + 
  geom_col(
    aes(fill=type)
  )+ 
  geom_text(
    aes(label=glue::glue("{val}%",val=round(pourcentage,1))),
    position = position_stack(vjust = 0.5)
  ) 


```

Enfin, le graphique ci-dessous donne des éléments sur l'occupation des


```{r}
#| label: fig-6
#| fig-cap: !expr "sprintf('Quelle occupation pour les logements autorisées entre %s et %s', annee_debpttprd, annee_etu)"
dp_gc_aut_resprincipousecond(rlgts_dom, deb=annee_debpttprd, fin=annee_etu) %>%   
  ggplot(aes(x=annee.aut, y = pourcentage, group = type)) + 
  geom_col(
    aes(fill=type)
  )+ 
  geom_text(
    aes(label=glue::glue("{val}%",val=round(pourcentage,1))),
    position = position_stack(vjust = 0.5)
  ) 
```




En [`r annee_etu`]{.publi-date}  , [`r s.aut.nb.ppal()`]{.publi-data} % 
des logements autorisés serviront de résidence principale.








## Principaux résultats sur les logements commencés

Voyons à présent les résultats pour les logements commencés :


```{r}
#| label: tbl-3
#| tbl-cap: !expr 'sprintf("Nombre de logements commencés entre %d et %d", annee_debgrdprd, annee_etu)'

data4kable <- dp_tb_com_compare_hexagone(indicateurs_annuels_dom_com,annee_debgrdprd,annee_etu,indicateurs_annuels_territoire_national_horsMayotte_com)


data4kable %>% 
  knitr::kable(
    col.names = append(c(""), colnames(data4kable)[2:4]),
    format.args = list(
      big.mark = " ",
      digits = 2
    )
  )
```




```{r}
#| label: fig-7
#| fig-cap: !expr 'sprintf("Evolution du nombre de logements commencés entre %d et %d", annee_debgrdprd, annee_etu)'

chrb100 <- dp_gl_com_compare_hexagone(
  indicateurs_annuels_dom_com,
  indicateurs_annuels_territoire_national_horsMayotte_com,
  annee_debgrdprd, annee_etu,
  annee_base100=2013
)
  
ggplot() +
  geom_line(data = chrb100$dom, aes(x=annee, y=b100), color = "red") +
  geom_line(data = chrb100$hx, aes(x=annee, y=b100), color = "blue") +
  scale_x_continuous(breaks = seq(annee_debgrdprd,2022), minor_breaks = FALSE)


```


[Si la période post-crise ([2010]{.publi-date}  et [2011]{.publi-date} ) a vu 
le nombre de travaux augmenter, cette tendance semble révolue, comme en témoigne
les résultats sur le nombre de logements commencés depuis [`r annee_debpttprd`]{.publi-data} .
La forte chute des autorisations en [2016]{.publi-data}  a donc eu un impact
limité sur les logements commencés, même si une baisse est observée depuis
[`r annee_nm1`]{.publi-date}  . Ainsi, [`r s.com.nb.colres()`]{.publi-data}  logements collectifs
et [`r s.com.nb.ind()`]{.publi-data}  logements individuels ont été mis en chantier pendant 
l'année [`r annee_etu`]{.publi-date}.]{.publi-hightlight}






## La construction de logements locatifs sociaux


Le financement du logement social est organisé autour de la ligne budgétaire
unique. Les chiffres de [`r annee_nm1`]{.publi-date}  à [`r annee_etu`]{.publi-date}
du financement des Logements Locatifs Sociaux (LLS) et Logements Locatifs Très 
Sociaux (LLTS) sont donnés ci-dessous :



```{r}
#| label: tbl-4
#| tbl-cap: !expr "sprintf('Financement des LLS et LLTS de %s à %s', annee_debpttprd, annee_etu)"
dp_tb_rpls_financements(
  indicateurs_rpls_dom, 
  rpls_table_passage_financement_dom,
  annee_debpttprd,
  annee_etu
)
```



En [`r annee_etu`]{.publi-date}, 
[`r r.nb.LLS()`]{.publi-data} LLS et 
[`r r.nb.LLTS()`]{.publi-data} LLTS ont été mis en chantier. 
Le nombre de livraisons 
fluctue plus fortement selon les délais de réalisation des opérations. 
Sur les trois dernières années, ces livraisons atteignent 
[`r r.nb.moy3a.lls_llts()`]{.publi-data} (LLS + LLTS) par an en moyenne.



```{r}
#| label: tbl-5
#| tbl-cap: !expr "sprintf('Nombre de logements sociaux mis en service de %s à %s', annee_debpttprd, annee_etu)"
dp_tb_rpls_mes(indicateurs_rpls_dom, annee_debpttprd, annee_etu)
```



Au 1er janvier [`r annee_etu`]{.publi-date}, 
le parc locatif des bailleurs sociaux compte 
[`r fmt(r.nb())`]{.publi-data} logements en Martinique, en progression de 
[`r r.nb.evol.prct()`]{.publi-data}% sur un an, 
soit [`r r.nb.evol()`]{.publi-data} logements supplémentaires.

[Comme l'année précédente en Outre-Mer, c'est en Guyane et 
à la Réunion que ce taux est le plus élevé (autours des + 4 % chacune), 
contre +1,5 % en France métropolitaine. Ainsi, les DOM sont parmi 
les régions les plus dynamiques sur l'augmentation de leurs parcs 
de logements sociaux.]{.publi-hightlight}

[Le tableau ci-dessous illustre la dynamique positive des dernières années 
sur l'augmentation du parc social. En effet, en moyenne de 2003 à 2013, 
le parc a évolué de l'ordre de + 500 nouveaux logements par an.]{.publi-hightlight} 

[Ce chiffre a doublé sur la période 2014-2018.]{.publi-hightlight}

Le tableau ci-dessous illustre la dynamique positive des dernières années 
sur l'augmentation du parc social. 
En effet, en moyenne de  [`r annee_debpttprd`]{.publi-date} à  [`r annee_etu`]{.publi-date}, 
le parc a évolué de l'ordre de 
[`r r.accr.moy_pttprd()`]{.publi-data} nouveaux logements par an.


```{r}
#| label: tbl-6
#| tbl-cap: !expr "sprintf('Parc des logements sociaux de %s à %s', annee_debpttprd, annee_etu)"
dp_tb_rpls_collind(indicateurs_rpls_dom, annee_debpttprd, annee_etu)
```


```{r}
#| label: extra
#| eval: false

ctrl_rpls_mes_VS_accroissement_parc(indicateurs_rpls_dom, annee_debpttprd, annee_etu)

```


Les détails sur le parc de logement locatif social sont donnés dans la 
[publication sur RPLS](http://www.martinique.developpement-durable.gouv.fr/le-parc-locatif-des-bailleurs-sociaux-rpls-a331.html).


